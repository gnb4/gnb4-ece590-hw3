//
//  StudentTVC.swift
//  ECE practice
//
//  Created by Gilbert Brooks on 2/5/17.
//  Copyright © 2017 Duke University. All rights reserved.
//

import UIKit

class StudentTVC: UITableViewController {
    
    @IBOutlet weak var userImage: UIImageView!
//    
//    var students: [Student] { //computed property
//        
//        var studTeam = StudentTeam.studentTeam()
//        return studTeam[0].students
//        
//    }
    
    
    
    
    lazy var myTeam: [StudentTeam] = {
        return StudentTeam.studentTeam()
    }() //because this is quite large, use it lazily.
   
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //let line = students[section]
        return "Fava Team"
    }
    
  
    override func viewDidLoad() {
        
        //        tableView.delegate = self
        //        tableView.dataSource = self
        //
        
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        let pL = myTeam[section]
        return pL.students.count
        
        
        
        //return myTeam.count
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! StudentTableViewCell
        
        let roxySect = myTeam[indexPath.section]
        let stud = roxySect.students[indexPath.row]
        //set properties of the cell
        
        
        
//        cell.textLabel?.text = product.name
//        cell.detailTextLabel?.text = product.origin
//        cell.imageView?.image = product.picture
        cell.configureCellWith(student: stud)
        
        return cell
        
        
    }
    


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier{
            switch identifier {
            case "EditCell":
                print(2)
                let DetailVC = segue.destination as? DetailViewController
                
//                if let indexPath = self.tableView.indexPathForSelectedRow(sender as UITableViewCell){
//                    ProductDetailViewController.product = productAtIndexPath(indexPath)
//                }
                
                if let indexPath = self.tableView.indexPath(for: sender as! UITableViewCell) {
                    DetailVC?.person = studentAtIndexPath(indexPath: indexPath as NSIndexPath)
                }
                
            case "Show Edit" :
                let editTableVC = segue.destination as? EditTableViewController
                if let indexPath = self.tableView.indexPath(for: sender as! UITableViewCell){
                    editTableVC?.s = studentAtIndexPath(indexPath: indexPath as NSIndexPath)
                
                }
            default:
                break
            }
        }
    }
    
    
    func studentAtIndexPath(indexPath: NSIndexPath) -> Student {
        let mates = myTeam[indexPath.section]
        return mates.students[indexPath.row]
        

    }
    
    
    @IBAction func unwindSeg(segue: UIStoryboardSegue) {
        
        let source: ViewController = segue.source as! ViewController
        if let item: Student = source.person {
    
        
        myTeam[0].students.append(item)
        
        self.tableView.reloadData()
        }
        
        //check to do list for segue format
        //get student class form ViewController
        //call append from StudentTeam class
        
    }
    
    
    
    
    
}
