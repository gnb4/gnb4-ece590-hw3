//
//  StudentTableViewCell.swift
//  ECE practice
//
//  Created by Gilbert Brooks on 2/12/17.
//  Copyright © 2017 Duke University. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

    @IBOutlet weak var picture: UIImageView!

    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var subtext: UILabel!

    func configureCellWith(student: Student){
        ///picture.image = student.picture
        name.text = student.name
        subtext.text = student.fromWhere
        picture.image = student.studentPhoto

        
    }

}
