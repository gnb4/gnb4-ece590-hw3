//
//  Student.swift
//  ECE practice
//
//  Created by Gilbert Brooks on 2/6/17.
//  Copyright © 2017 Duke University. All rights reserved.
//

///-- MODEL
import Foundation
import UIKit

//class Student{
//    
//    var name, origin: String
//    
//    var picture: UIImage
//    
//    init(named:String, hometown: String, pic: UIImage) {
//
//        self.name = named
//        self.origin = hometown
//        self.picture = pic
//    }

    
//}



class Student: Person, CustomStringConvertible {
    
    
    var degree: String = "no school"
    var csLanguages = [String]() //constructor of type
    
    init(_name: String, _fromWhere: String, _gender: String, _hobbies: String, _age: String, _degree: String, _csLanguages: [String], _studentPhoto: UIImage) {
        
        self.degree = _degree
        self.csLanguages = _csLanguages
        
        super.init(name: _name, fromWhere: _fromWhere, gender: _gender, hobbies: _hobbies, age:_age, studentPhoto: _studentPhoto)
        
        
    }
    
    
    /*
     override init()
     {
     
     /// get rid of hard code
     super.init(name: "Gilbert Brooks", fromWhere: "Shelby, NC", degree: "AAAS and CS", gender: "Male", hobbies: ["swift-programming", "designing", "reading"], csLanguages: ["java":1, "python": 1, "swift": 1], age:22.0)
     }*/
    
    let isStudent = true
    
    func printHobbies() -> String {
        //let num = 40
        let aString = ""
        
//        if (!self.hobbies.isEmpty) && (self.age>20.0) && (isStudent == true){
//            aString += self.hobbies
//            
//        }else{
//            print("not enough hobbies.")
//        }
        
        return aString
    }
    
    
    
    var description: String {
        return ("\(self.name) is from \(self.fromWhere) and he is \(self.age) years old. His hobbies are\(self.printHobbies()). \(self.name) is a \(self.gender) and he studies \(self.degree) at Duke University. His Computer Sciences languages include\(csLanguages).")
    }
}
