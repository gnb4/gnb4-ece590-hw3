//
//  Person.swift
//  ECE practice
//
//  Created by Gilbert Brooks on 2/15/17.
//  Copyright © 2017 Duke University. All rights reserved.
//

import Foundation
import UIKit
class Person {
    
    var name = "no name"
    var fromWhere : String = "no residence"
    var gender = "no gender"
    var hobbies: String = "no hobbies"
    var age = "0.0"
    var studentPhoto = UIImage(named: "alex")
    init() {}
    
    init(name: String, fromWhere: String, gender: String, hobbies: String, age: String, studentPhoto: UIImage)
    {
        self.name = name
        self.fromWhere = fromWhere
        self.gender = gender
        self.hobbies = hobbies
        self.age = age
        self.studentPhoto = studentPhoto
        
    }
}
