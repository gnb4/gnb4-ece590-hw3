//
//  EditTableViewController.swift
//  ECE practice
//
//  Created by Gilbert Brooks on 2/7/17.
//  Copyright © 2017 Duke University. All rights reserved.
//

import UIKit

class EditTableViewController: UITableViewController {
    
    
    var s: Student?
    
    
//    
//    lazy var teammates: [Student] = {
//        var stud = StudentTeam.studentTeam()
//        return stud[0].students
//    }() // only instantiated when needed
    
    @IBOutlet weak var studentPhoto: UIImageView!
    
    @IBOutlet weak var studentData: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ///check that there is a picture
        ///studentPhoto.image = s?.picture
        studentData.text = s?.description
        studentPhoto.image = s?.studentPhoto
        
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    
    
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "EditCell", for: indexPath)
//        
//        let studentCell = StudentTeam.allStuds().students[indexPath.row]
//        
//        //set properties of the cell
//        
//        
//        cell.imageView?.image = studentCell.csLanguages
//        
//        return cell
//
//    }
    

}
