//
//  ViewController.swift
//  ECE practice
//
//  Created by Gilbert Brooks on 1/30/17.
//  Copyright © 2017 Duke University. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    
    ///prepare for seg will go here
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.saveStudent()
        return

        
        
        
    }
    /*
     
    1. save button called,
    2. prepare for seg saves the info to our global student var,
    3. our unwind is called in TVC, and reload is called within that
    
    */
    
    var picker = UIImagePickerController()
    
    lazy var theTeam: [StudentTeam] = {
        return StudentTeam.studentTeam()
    }()
    
    
    var person: Student?
    
    //we'll create our new student

    
    
    @IBOutlet weak var myImageView: UIImageView!
    @IBOutlet weak var userDescription: UITextView!
    @IBOutlet weak var gender: UITextField!
    @IBOutlet weak var age: UITextField!
    @IBOutlet weak var fromWhere: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var langs: UITextField!
    @IBOutlet weak var hobbies: UITextField!
    @IBOutlet weak var degree: UITextField!
    
    var myImagePicker = UIImagePickerController()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.allowsEditing = true
        
        picker.delegate = self
        
    }
    
    
    
    @IBAction func photoFromLibrary(_ sender: UIBarButtonItem) {
        
        picker.allowsEditing = false
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.modalPresentationStyle = .popover
        present(picker, animated: true, completion: nil)
        picker.popoverPresentationController?.barButtonItem = sender
    
    
    
    }
    
    @IBAction func shootPhoto(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker,animated: true,completion: nil)
        } else {
            noCamera()
        }
    }
    func noCamera(){
        let alertVC = UIAlertController( title: "No Camera", message: "Sorry, this device has no camera", preferredStyle: .alert)
        
        
        let okAction = UIAlertAction(title: "OK", style:.default, handler: nil)
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
    
    
    //MARK: - Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        
        print("almost")
        var  chosenImage = UIImage()
        chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //2
        myImageView.contentMode = .scaleAspectFit //3
        
        
        
        print("i made it")
        
        
        
        
        myImageView.image = chosenImage //4
        dismiss(animated:true, completion: nil) //5
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
//    @IBAction func makeSense(_ sender: Any) {
//        
//        
//        if (name.text?.isEmpty)! || (fromWhere.text?.isEmpty)! || (langs.text?.isEmpty)! || (degree.text?.isEmpty)! || (age.text?.isEmpty)! || (hobbies.text?.isEmpty)!
//        {return}
//        
//        userDescription.text = "\(name.text!) is from \(fromWhere.text!) and enjoys \(hobbies.text!). \(name.text!) is \(age.text!) years old, and has experience with \(langs.text!)... )"
//        
//        
//    }
    
    
    /*
 
     create student
     
 */
    
    
    
    
    ///////
//    
//    class Person {
//        
//        var name = "no name"
//        var fromWhere : String = "no residence"
//        var gender = "no gender"
//        var hobbies: String = "no hobbies"
//        var age = 0.0
//        
//        init() {}
//        
//        init(name: String, fromWhere: String, gender: String, hobbies: String, age: Double)
//        {
//            self.name = name
//            self.fromWhere = fromWhere
//            self.gender = gender
//            self.hobbies = hobbies
//            self.age = age
//            
//        }
//    }
//    
//    
//    
//    class Student: Person, CustomStringConvertible {
//        
//        
//        var degree: String = "no school"
//        var csLanguages = [String]() //constructor of type
//
//        init(_name: String, _fromWhere: String, _gender: String, _hobbies: String, _age: Double, _degree: String, _csLanguages: [String]) {
//            
//            self.degree = _degree
//            self.csLanguages = _csLanguages
//            
//            super.init(name: _name, fromWhere: _fromWhere, gender: _gender, hobbies: _hobbies, age: _age)
//            
//            
//        }
//        
//        
//        /*
//        override init()
//        {
//            
//            /// get rid of hard code
//            super.init(name: "Gilbert Brooks", fromWhere: "Shelby, NC", degree: "AAAS and CS", gender: "Male", hobbies: ["swift-programming", "designing", "reading"], csLanguages: ["java":1, "python": 1, "swift": 1], age:22.0)
//        }*/
//        
//        let isStudent = true
//        
//        func printHobbies() -> String {
//            let num = 40
//            var aString = ""
//            
//            if (!self.hobbies.isEmpty) && (self.age>20.0) && (isStudent == true){
//                aString += self.hobbies
//                
//            }else{
//                print("not enough hobbies.")
//            }
//            
//            return aString
//        }
//        
//        
//        
//        var description: String {
//            return ("\(self.name) is from \(self.fromWhere) and he is \(self.age) years old. His hobbies are\(self.printHobbies()). \(self.name) is a \(self.gender) and he studies \(self.degree) at Duke University. His Computer Sciences languages include\(csLanguages).")
//        }
//    }
    
    var myStudents = [String]()
    
    //var thisStudent = Student() // instantiating object
    
//    func printInfo(forName: String) -> String { //search in array forName  for match
//        myStudents.append("\(thisStudent.age)")
//        myStudents.append("\(thisStudent.name)")
//        if myStudents.contains(forName){
//            var description: String {
//                
//                return ("\(thisStudent.name) is from \(thisStudent.fromWhere) and he is \(thisStudent.age) years old. His hobbies are\(thisStudent.printHobbies()). \(thisStudent.name) is a \(thisStudent.gender) and he studies \(thisStudent.degree) at Duke University. His Computer Sciences languages include\(thisStudent.printExperience()).")
//            }
//            //myLabel?.text = thisStudent.description
//            print("123")
//            return thisStudent.description
//        }
//        return "student not found"
//    }
    
//    @IBAction func btnHit(_ sender: Any) {
//        
//        if name.text == "Gilbert Brooks" {
//            userDescription.text = printInfo(forName: "Gilbert Brooks")
//            print(printInfo(forName: "Gilbert Brooks"))
//            print(123)
//        }else{
//            userDescription.text = "student not found"
//        }
//    }
    
    
     func saveStudent() { /// have some alert letting users know whats happening
        
        if ((gender.text?.isEmpty)! || (name.text?.isEmpty)!) || (fromWhere.text?.isEmpty)! || (langs.text?.isEmpty)! || (degree.text?.isEmpty)! || (age.text?.isEmpty)! || (hobbies.text?.isEmpty)!
        {return}
        
        person = Student(_name: name.text!, _fromWhere: fromWhere.text!, _gender: gender.text!, _hobbies: hobbies.text!, _age: age.text! , _degree: degree.text!, _csLanguages: [langs.text!], _studentPhoto: myImageView.image!)
        
        
        
//        person?.name = name.text!
//        person?.degree = degree.text!
//        person?.csLanguages = [langs.text!]
//        person?.age = age.text!
//        person?.fromWhere = fromWhere.text!
//        person?.hobbies = hobbies.text!
//        
        
        
        
        
    }
    
//    
//    class func allStuds() -> StudentTeam {
//        var per = [Student]()
//        //per.append(contentsOf: <#T##Collection#>)
//        per.append(student)
//        
//        per.append(Student(named: "Gilbert Brooks", hometown: "Gilbert Brooks is from Shelby, NC and he is 22.0 years old. His hobbies are swift-programming designing reading. Gilbert Brooks is a Male and he studies AAAS and CS at Duke University. His Computer Sciences languages include java for 1 year(s) swift for 1 year(s) python for 1 year(s).", pic: UIImage(named: "gilbertPicture")!))
//    }
//    
//    
//    func saveBtnTapped(_ sender: Any) -> StudentTeam {
//        
//        var mate = [Student]()
//        person?.name = ""
//        
//        
//            //print(theTeam.count)
//       
//        print(StudentTeam.allStuds().students.count) //append person to this
////        enrolled.append(Student())
////
////        return StudentTeam(named: "allStuds", includeStudents: mate)
//    
////        return
//    }
    
    
    
//            theTeam.append()
        
//        tableView.beginUpdates()
//        tableView.insertRows(at: [IndexPath(row: yourArray.count-1, section: 0)], with: .automatic) //append here
//        tableView.endUpdates()
        
        
        //StudentTeam.allStuds().students.append(Element)
        
    
    
    
    
    
    
    ///print(printInfo(forName: "Gilbert Brooks"))
    
    //: *END OF YOUR CODE - YOU MUST USE THE FUNCTION TYPE DEFINED BELOW.*
    //: UNCOMMENT THE LINE BELOW AND ENTER YOUR NAME TO TEST YOUR CODE
    
    ///print(printInfo(forName: "Gilbert Brooks"))  // type is "(String) -> String"
    
    
}


////////





























