//
//  StudentTeam.swift
//  ECE practice
//
//  Created by Gilbert Brooks on 2/6/17.
//  Copyright © 2017 Duke University. All rights reserved.
//
///here
import Foundation

import UIKit
class StudentTeam{
    
    
    var name: String
    var students : [Student]
    var pic: UIImage
    
    init(named: String, includeStudents: [Student]){
        
        name = named
        students = includeStudents
        pic = UIImage(named: "alex")!
    }
    
    ///Here
    class func studentTeam() -> [StudentTeam]{
        return [self.allStuds()]

    }
    
   class func allStuds() -> StudentTeam { //change format to more general student class
        
        var students = [Student]()
        //var studentPhoto = UIImage(named: "alex")
    
    students.append(Student(_name: "Gilbert", _fromWhere: "Shelby, NC", _gender: "male", _hobbies: "football and wrestling", _age: "88", _degree: "CS", _csLanguages: ["1","2","3",], _studentPhoto: UIImage(named: "gilbertPicture")!))
    
    students.append(Student(_name: "Alex", _fromWhere: "Longon, UK", _gender: "male", _hobbies: "football and wrestling", _age: "88", _degree: "CS", _csLanguages: ["1","2","3",], _studentPhoto: UIImage(named: "alex")!))
    
    students.append(Student(_name: "John", _fromWhere: "Houston, TX", _gender: "male", _hobbies: "football and wrestling", _age: "88", _degree: "CS", _csLanguages: ["1","2","3",], _studentPhoto: UIImage(named: "john")!))
    
        
        
        return StudentTeam(named: "allStuds", includeStudents: students)
        
    }
    
    
    
    func addToTeam(s: Student) {
        students.append(s)
    }
    
    
    
    
    
    
    
 ///video way below... didn't work for me.
//    private class func GilbertBrooks() -> StudentTeam {
//        
//        var students = [Student]()
//        students.append(Student(named: "Gil", hometown: "Shelby", pic: UIImage(named:"gilbertPicture")!))
//        
//        return StudentTeam(named: "GilbertBrooks", includeStudents: students)
//    }
//    
//    
//    private class func Alex() -> StudentTeam {
//        
//        var students = [Student]()
//        students.append(Student(named: "Alex", hometown: "Shelby", pic: UIImage(named:"gilbertPicture")!))
//
//        return StudentTeam(named: "GilbertBrooks", includeStudents: students)
//    }
//    
//    private class func John() -> StudentTeam {
//        
//        var students = [Student]()
//        students.append(Student(named: "Alex", hometown: "Shelby", pic: UIImage(named:"gilbertPicture")!))
//
//        return StudentTeam(named: "GilbertBrooks", includeStudents: students)
//    }
//
}
